const palabrasNoValidas = ["clavo", "pablito"];
const parrafo = "PabliTo claVo un clavito, que ClaviTo clavo Pablito";

const corte = (trabalenguas) => {
    const minusculas = trabalenguas.toLowerCase()
    const cortado = minusculas.match('clavito, que clavito');
    return cortado[0]
}
console.log(corte(parrafo));

// closure
const CrearContador = () =>{

    let contador = 0;
    setTimeout(() => {
        contador=100;

    },5000)
    return  incrementar = () => {
        contador = contador + 1;
        return contador
    }
}

const cuentas = CrearContador();

// callback
const saludar = () => {
    console.log('hola')
}
const saludo = (fn) => {
    fn()
}
saludo(saludar);
nombre()
function nombre(){ console.log('latuaro')}

let counter = 0;
const decirHola= () =>{
    console.log(+1);
    counter ++;

    (counter === 3 ) ? element.removeEventListener('click', decirHola):console.log('nofunciono')
}
const element = document.getElementById('seg');
element.addEventListener('click', decirHola);

const error = () => {
    console.log('error')
}

    let x;
    const promesa = new Promise((resolve, reject) => {
        setTimeout(() => {
            x = 2 + 3 ;
            console.log('proceso terminado')
            resolve(x)
            reject(error)
        }, 2000);
        
    })
    console.log('iniciando proceso')

    promesa.then(res => {
        console.log('resultado ', res);
    })

    const Promesa1 = new Promise ((resolve, reject) => {
        resolve('resuelta 1');
    })
    const Promesa2 = new Promise((resolve,reject) => {
        setTimeout(() => {
            resolve('resuelta 2')
        }, 1000);
    })
    const Promesa3 = new Promise ((resolve,reject) => {
        setTimeout(() => {
            resolve('resuelto3')
        },2000);
    })

    Promise.all([Promesa1,Promesa2,Promesa3])
                      .then(values => {
                          console.log(values);
                      })